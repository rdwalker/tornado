## [1.3.3](https://gitlab.com/rdwalker/tornado/compare/v1.3.2...v1.3.3) (2023-01-31)


### Bug Fixes

* **RouterProvider:** Add fallback element ([ae5f58f](https://gitlab.com/rdwalker/tornado/commit/ae5f58fea1bfbdf11d0115385506f088e806fc14))
* **Routes:** Add basename ([e086270](https://gitlab.com/rdwalker/tornado/commit/e0862707de0d79e652765ccd2559e361dcb59372))

## [1.3.2](https://gitlab.com/rdwalker/tornado/compare/v1.3.1...v1.3.2) (2023-01-30)


### Bug Fixes

* **LoadingElement:** Set a higher zIndex to show above form inputs and other page elements. ([23bfef9](https://gitlab.com/rdwalker/tornado/commit/23bfef93a2c392a64217ccb898ddb8b45f54fed7))

## [1.3.1](https://gitlab.com/rdwalker/tornado/compare/v1.3.0...v1.3.1) (2023-01-30)


### Bug Fixes

* **NameForm:** fix unhandled error in mutation. ([fbe37b5](https://gitlab.com/rdwalker/tornado/commit/fbe37b530a50f00e45422ffb8926533137662e53))

# [1.3.0](https://gitlab.com/rdwalker/tornado/compare/v1.2.0...v1.3.0) (2023-01-30)


### Bug Fixes

* **msw:** Do not start MSW in production. ([eed9116](https://gitlab.com/rdwalker/tornado/commit/eed91169f1738ff9ffca582e3993b2542f8b3439))


### Features

* **NameForm:** Setup mock API to submit the form and add Types to mock handlers. ([391b0bc](https://gitlab.com/rdwalker/tornado/commit/391b0bcf748d4ae385e90923da1e14e8b7450d0e))

# [1.2.0](https://gitlab.com/rdwalker/tornado/compare/v1.1.1...v1.2.0) (2023-01-28)


### Features

* setup MSW and code-splitting ([9b628a1](https://gitlab.com/rdwalker/tornado/commit/9b628a1588d4ea5c3041bb90f3e48dc70c2ab039))

## [1.1.1](https://gitlab.com/rdwalker/tornado/compare/v1.1.0...v1.1.1) (2023-01-18)


### Bug Fixes

* **ThemeProvider:** fix infinite rerender bug ([e6fffb1](https://gitlab.com/rdwalker/tornado/commit/e6fffb1b487167594e77b0708b76e3c45208328a))

# [1.1.0](https://gitlab.com/rdwalker/tornado/compare/v1.0.0...v1.1.0) (2023-01-18)


### Features

* **storybook:** added storybook to project ([cedd1af](https://gitlab.com/rdwalker/tornado/commit/cedd1af65daa5260e88f680d5a602ad6da8c6913))

# 1.0.0 (2022-12-30)


### Bug Fixes

* **ci:** remove GITLAB_TOKEN variable ([8dc727f](https://gitlab.com/rdwalker/tornado/commit/8dc727f1809d03443d07a4705820db1d094659a0))
