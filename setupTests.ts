import '@testing-library/jest-dom';
import { extend } from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import utc from 'dayjs/plugin/utc';

import { options } from './src/mocks/options';
import { server } from './src/mocks/server';

beforeAll(() => {
  extend(relativeTime);
  extend(utc);
  server.listen(options);
});

afterEach(() => server.resetHandlers());

afterAll(() => server.close());
