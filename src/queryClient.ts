import type { QueryFunctionContext } from '@tanstack/react-query';
import { QueryCache, QueryClient } from '@tanstack/react-query';
import { toast } from 'react-hot-toast';

import { assertIsError } from './guards/guards';

async function defaultQueryFn({
  queryKey,
  signal,
}: QueryFunctionContext): Promise<unknown> {
  const response = await fetch(`/api/${queryKey.join('/')}`, {
    signal,
  });
  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return response.json();
}

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      queryFn: defaultQueryFn,
      useErrorBoundary: (_error, query): boolean =>
        query.state.data === undefined,
    },
  },
  queryCache: new QueryCache({
    onError: (error, query): void => {
      if (query.state.data !== undefined) {
        assertIsError(error);
        toast.error(`Something went wrong: ${error.message}`);
      }
    },
  }),
});

export { queryClient };
