export interface Data {
  id: string;
  name: string;
}

export interface MessageData {
  message: string;
}
