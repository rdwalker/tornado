import type { ReactElement } from 'react';

import { Box, Stack } from '@mui/material';
import { Outlet, useNavigation } from 'react-router-dom';

import AppBar from './components/Layout/AppBar/AppBar';
import NavDrawer, {
  DrawerHeader,
} from './components/Layout/NavDrawer/NavDrawer';
import LoadingElement from './components/LoadingElement/LoadingElement';
import { useToggle } from './hooks';

function App(): ReactElement {
  const { value: open, setValueTrue, setValueFalse } = useToggle();
  const { state } = useNavigation();

  return (
    <Box sx={{ display: 'flex', flexGrow: 1 }}>
      <AppBar onDrawerOpenClick={setValueTrue} open={open} />
      <NavDrawer onDrawerCloseClick={setValueFalse} open={open} />
      <Stack component="main" sx={{ flexGrow: 1 }}>
        <DrawerHeader />
        <LoadingElement open={state === 'loading'} />
        <Stack flexGrow={1} p={3}>
          <Outlet />
        </Stack>
      </Stack>
    </Box>
  );
}

export default App;
