import type { ReactElement } from 'react';

import type { BackdropProps } from '@mui/material';
import { Backdrop, CircularProgress } from '@mui/material';

function LoadingElement(backdropProps: Partial<BackdropProps>): ReactElement {
  return (
    <Backdrop
      open
      sx={{ zIndex: (theme) => theme.zIndex.appBar }}
      {...backdropProps}
    >
      <CircularProgress color="inherit" />
    </Backdrop>
  );
}
export default LoadingElement;
