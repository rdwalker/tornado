import type { ReactElement } from 'react';

import { Typography } from '@mui/material';
import { useRouteError } from 'react-router-dom';

import { isError, isResponse } from '../../guards/guards';

function ErrorElement(): ReactElement {
  const error = useRouteError();

  return (
    <div id="error-page">
      <Typography variant="h5">Oops!</Typography>
      <Typography>Sorry, an unexpected error has occurred.</Typography>
      <Typography>
        <i>
          {isResponse(error)
            ? error.statusText
            : isError(error)
            ? error.message
            : 'Unknown Error'}
        </i>
      </Typography>
    </div>
  );
}

export default ErrorElement;
