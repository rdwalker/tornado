import type { Meta, StoryObj } from '@storybook/react';

import TrimmedTextField from './TrimmedTextField';

export default {
  title: 'Tornado/Trimmed Text Field',
  parameters: {
    docs: {
      description: {
        component:
          'A MUI TextField that will automatically trim the input value on blur.',
      },
    },
  },
  component: TrimmedTextField,
  argTypes: {
    color: {
      control: 'select',
      options: ['primary', 'secondary', 'error', 'warning', 'success', 'info'],
    },
    size: {
      control: 'radio',
      defaultValue: 'medium',
      options: ['small', 'medium'],
    },
  },
} as Meta<typeof TrimmedTextField>;

export const KitchenSink: StoryObj<typeof TrimmedTextField> = {
  args: {
    label: 'Value',
    color: 'primary',
    placeholder: '',
    helperText: '',
    error: false,
    autoFocus: true,
    size: 'medium',
    disabled: false,
    fullWidth: false,
    multiline: false,
  },
};

export const Error: StoryObj<typeof TrimmedTextField> = {
  args: {
    ...KitchenSink.args,
    autoFocus: false,
    error: true,
    helperText: 'An error occurred',
  },
};
