import type { FocusEventHandler, ReactElement } from 'react';
import { forwardRef } from 'react';

import type { TextFieldProps } from '@mui/material';
import { TextField } from '@mui/material';

interface TrimmedTextFieldProps
  extends Omit<TextFieldProps, 'value' | 'defaultValue'> {
  value?: string;
  defaultValue?: string;
}

const TrimmedTextField = forwardRef<HTMLDivElement, TrimmedTextFieldProps>(
  ({ onBlur, onChange, ...props }, ref): ReactElement => {
    const handleBlur: FocusEventHandler<HTMLInputElement> = (e): void => {
      e.target.value = e.target.value.trim();
      onChange?.(e);
      onBlur?.(e);
    };

    return (
      <TextField ref={ref} {...props} onBlur={handleBlur} onChange={onChange} />
    );
  },
);
TrimmedTextField.displayName = 'TrimmedTextField';

export default TrimmedTextField;
