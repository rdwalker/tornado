import type { ReactElement } from 'react';

import { zodResolver } from '@hookform/resolvers/zod';
import { LoadingButton } from '@mui/lab';
import { Box, Stack, Typography } from '@mui/material';
import { useMutation } from '@tanstack/react-query';
import { Controller, useForm } from 'react-hook-form';
import { toast } from 'react-hot-toast';
import { z } from 'zod';

import { assertIsError } from '../../guards/guards';
import TrimmedTextField from './Fields/TrimmedTextField';

interface nameFormData {
  firstName: string;
  lastName: string;
}

const validationSchema = z.object({
  firstName: z.string().min(1),
  lastName: z.string().min(1),
});

async function postNewName({
  firstName,
  lastName,
}: nameFormData): Promise<null> {
  const formData = new FormData();
  formData.append('firstName', firstName);
  formData.append('lastName', lastName);
  const response = await fetch('/api/name/', {
    method: 'post',
    body: formData,
  });
  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return null;
}

function NameForm(): ReactElement {
  const {
    control,
    handleSubmit,
    formState: { isSubmitting, isValid },
  } = useForm({
    resolver: zodResolver(validationSchema),
    defaultValues: {
      firstName: '',
      lastName: '',
    },
  });

  const { mutateAsync: sendNewName } = useMutation({
    mutationFn: postNewName,
  });

  const onSubmit = handleSubmit(async (data) => {
    try {
      await sendNewName(data);
      toast.success('Name created');
    } catch (error) {
      assertIsError(error);
      toast.error(error.message);
    }
  });

  return (
    <Stack
      component="form"
      onSubmit={(e): void => {
        void onSubmit(e);
      }}
      spacing={2}
    >
      <Typography variant="h6">Example Form</Typography>
      <Controller
        control={control}
        name="firstName"
        render={({ field, fieldState: { error } }): ReactElement => (
          <TrimmedTextField
            {...field}
            disabled={isSubmitting}
            error={!!error}
            helperText={error?.message}
          />
        )}
      />
      <Controller
        control={control}
        name="lastName"
        render={({ field, fieldState: { error } }): ReactElement => (
          <TrimmedTextField
            {...field}
            disabled={isSubmitting}
            error={!!error}
            helperText={error?.message}
          />
        )}
      />
      <Box display="flex" justifyContent="end">
        <LoadingButton
          disabled={!isValid}
          loading={isSubmitting}
          type="submit"
          variant="outlined"
        >
          Submit
        </LoadingButton>
      </Box>
    </Stack>
  );
}

export default NameForm;
