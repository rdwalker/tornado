import type { ReactElement } from 'react';

import { Alert, SnackbarContent } from '@mui/material';
import type { Renderable, ToastType } from 'react-hot-toast';

interface ToastBarContentProps {
  type: ToastType;
  icon: Renderable;
  message: Renderable;
}

function ToastBarContent({
  icon,
  message,
  type,
}: ToastBarContentProps): ReactElement {
  switch (type) {
    case 'success':
    case 'error':
      return (
        <Alert icon={icon} severity={type}>
          {message}
        </Alert>
      );
    case 'loading':
      return (
        <Alert icon={icon} severity="info">
          {message}
        </Alert>
      );
    case 'blank':
    case 'custom':
    default:
      return <SnackbarContent message={message} />;
  }
}

export default ToastBarContent;
