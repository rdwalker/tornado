import type { ReactElement } from 'react';

import { Fade } from '@mui/material';
import type { Toast } from 'react-hot-toast';
import { ToastBar as OriginalToastBar } from 'react-hot-toast';

import ToastBarContent from './ToastBarContent';

function ToastBar({ toast }: { toast: Toast }): ReactElement {
  return (
    <Fade in={toast.visible}>
      <div>
        <OriginalToastBar
          style={{ ...toast.style, padding: 0, animation: 'unset' }}
          toast={toast}
        >
          {({ icon, message }): ReactElement => (
            <ToastBarContent icon={icon} message={message} type={toast.type} />
          )}
        </OriginalToastBar>
      </div>
    </Fade>
  );
}

export default ToastBar;
