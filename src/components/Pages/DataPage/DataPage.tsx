import type { ReactElement } from 'react';

import { Stack } from '@mui/material';
import type { GridColDef } from '@mui/x-data-grid';
import { DataGrid } from '@mui/x-data-grid';
import { useQuery } from '@tanstack/react-query';

import { useLoaderData } from '../../../hooks';
import type { Data } from '../../../types/types';
import { dataQueryKey } from './queryInfo';

const columns: GridColDef<Data>[] = [
  {
    field: 'id',
    headerName: 'ID',
  },
  {
    field: 'name',
    headerName: 'Entry',
    flex: 1,
  },
];

function DataPage(): ReactElement {
  const { data: initialData } = useLoaderData<Data[]>();
  const { data } = useQuery({
    queryKey: [dataQueryKey],
    initialData,
  });

  return (
    <Stack flexGrow={1} width="100%">
      <DataGrid
        columns={columns}
        initialState={{
          pagination: {
            paginationModel: {
              pageSize: 25,
            },
          },
        }}
        rows={data}
      />
    </Stack>
  );
}

export default DataPage;
