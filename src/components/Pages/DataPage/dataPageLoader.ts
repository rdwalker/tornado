import type { Data } from '../../../types/types';
import { createLoader } from '../../../utils/createLoader';
import { dataQueryKey } from './queryInfo';

export default createLoader<Data[]>(dataQueryKey);
