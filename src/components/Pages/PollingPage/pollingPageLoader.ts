import type { MessageData } from '../../../types/types';
import { createLoader } from '../../../utils/createLoader';
import { pollingQueryKey } from './queryInfo';

export default createLoader<MessageData>(pollingQueryKey, true);
