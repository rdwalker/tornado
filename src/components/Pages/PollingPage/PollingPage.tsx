import type { ReactElement } from 'react';

import { Typography } from '@mui/material';
import { useQuery } from '@tanstack/react-query';

import { useLoaderData } from '../../../hooks';
import type { MessageData } from '../../../types/types';
import LoadingElement from '../../LoadingElement/LoadingElement';
import { pollingQueryKey } from './queryInfo';

function PollingPage(): ReactElement {
  const { data: initialData } = useLoaderData<MessageData | undefined>();
  const { data } = useQuery<MessageData>({
    queryKey: [pollingQueryKey],
    refetchInterval: 5000,
    initialData,
  });

  if (data) {
    return <Typography>{data.message}</Typography>;
  }

  return <LoadingElement />;
}

export default PollingPage;
