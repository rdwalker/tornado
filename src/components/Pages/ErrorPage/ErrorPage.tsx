import type { ReactElement } from 'react';

import { Typography } from '@mui/material';
import { useQuery } from '@tanstack/react-query';

import { useLoaderData } from '../../../hooks';
import type { MessageData } from '../../../types/types';
import { errorQueryKey } from './queryInfo';

function ErrorPage(): ReactElement {
  const { data: initialData } = useLoaderData<MessageData>();
  const { data } = useQuery({
    queryKey: [errorQueryKey],
    initialData,
  });

  return <Typography>{data.message}</Typography>;
}

export default ErrorPage;
