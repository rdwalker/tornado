import type { MessageData } from '../../../types/types';
import { createLoader } from '../../../utils/createLoader';
import { errorQueryKey } from './queryInfo';

export default createLoader<MessageData>(errorQueryKey);
