import type { ReactElement } from 'react';

import { Brightness4, Brightness7, Menu } from '@mui/icons-material';
import {
  IconButton,
  AppBar as MuiAppBar,
  styled,
  Toolbar,
  Typography,
  useTheme,
} from '@mui/material';
import type { AppBarProps as MuiAppBarProps } from '@mui/material';
import { toast } from 'react-hot-toast';

import { useColorToggle } from '../../../context/ThemeProvider';
import { DRAWER_WIDTH } from '../NavDrawer/NavDrawer';

interface AppBarProps extends MuiAppBarProps {
  open?: boolean;
  onDrawerOpenClick?: VoidFunction;
}

const StyledAppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})<AppBarProps>(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: DRAWER_WIDTH,
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

function AppBar({ open, onDrawerOpenClick }: AppBarProps): ReactElement {
  const { toggleColorMode } = useColorToggle();
  const theme = useTheme();

  const toggleTheme = (): void => {
    toggleColorMode();
    toast(
      `Theme changed ${theme.palette.mode === 'light' ? 'dark' : 'light'} mode`,
    );
  };

  return (
    <StyledAppBar open={open} position="fixed">
      <Toolbar>
        <IconButton
          aria-label="open drawer"
          color="inherit"
          edge="start"
          onClick={onDrawerOpenClick}
          sx={{
            marginRight: 5,
            ...(open && { display: 'none' }),
          }}
        >
          <Menu />
        </IconButton>
        <Typography component="div" noWrap sx={{ flexGrow: 1 }} variant="h6">
          Tornado
        </Typography>
        <IconButton color="inherit" onClick={toggleTheme}>
          {theme.palette.mode === 'dark' ? <Brightness7 /> : <Brightness4 />}
        </IconButton>
      </Toolbar>
    </StyledAppBar>
  );
}

export default AppBar;
