import type { CSSProperties, ReactElement } from 'react';
import { forwardRef } from 'react';

import {
  ChevronLeft,
  ChevronRight,
  DataArray,
  ErrorOutline,
  Home,
  Poll,
} from '@mui/icons-material';
import type { CSSObject, Theme } from '@mui/material';
import {
  Divider,
  emphasize,
  IconButton,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Drawer as MuiDrawer,
  styled,
  useTheme,
} from '@mui/material';
import type { NavLinkProps as NavLinkBaseProps } from 'react-router-dom';
import { NavLink as NavLinkBase } from 'react-router-dom';

export const DRAWER_WIDTH = 240;

const openedMixin = (theme: Theme): CSSObject => ({
  width: DRAWER_WIDTH,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme: Theme): CSSObject => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

export const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const StyledDrawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  width: DRAWER_WIDTH,
  flexShrink: 0,
  whiteSpace: 'nowrap',
  boxSizing: 'border-box',
  ...(open && {
    ...openedMixin(theme),
    '& .MuiDrawer-paper': openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    '& .MuiDrawer-paper': closedMixin(theme),
  }),
}));

const NavLink = forwardRef<HTMLAnchorElement, NavLinkBaseProps>(
  (props, ref) => {
    const theme = useTheme();

    return (
      <NavLinkBase
        ref={ref}
        {...props}
        style={({ isActive, isPending }): CSSProperties | undefined =>
          isActive
            ? {
                backgroundColor: emphasize(
                  theme.palette.background.default,
                  0.2,
                ),
              }
            : isPending
            ? {
                backgroundColor: theme.palette.primary.main,
              }
            : undefined
        }
      />
    );
  },
);
NavLink.displayName = 'NavLink';

const links = [
  {
    title: 'Home',
    icon: <Home />,
    to: '/',
  },
  {
    title: 'Data',
    icon: <DataArray />,
    to: '/data',
  },
  {
    title: 'Error',
    icon: <ErrorOutline />,
    to: '/error',
  },
  {
    title: 'Poll',
    icon: <Poll />,
    to: '/poll',
  },
];

interface NavDrawerProps {
  open?: boolean;
  onDrawerCloseClick?: VoidFunction;
}

function NavDrawer({ open, onDrawerCloseClick }: NavDrawerProps): ReactElement {
  const theme = useTheme();

  return (
    <StyledDrawer open={open} variant="permanent">
      <DrawerHeader>
        <IconButton onClick={onDrawerCloseClick}>
          {theme.direction === 'rtl' ? <ChevronRight /> : <ChevronLeft />}
        </IconButton>
      </DrawerHeader>
      <Divider />
      <List>
        {links.map(({ title, icon, to }) => (
          <ListItem disablePadding key={title} sx={{ display: 'block' }}>
            <ListItemButton
              component={NavLink}
              sx={{
                minHeight: 48,
                justifyContent: open ? 'initial' : 'center',
                px: 2.5,
              }}
              to={to}
            >
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: open ? 3 : 'auto',
                  justifyContent: 'center',
                }}
              >
                {icon}
              </ListItemIcon>
              <ListItemText primary={title} sx={{ opacity: open ? 1 : 0 }} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </StyledDrawer>
  );
}

export default NavDrawer;
