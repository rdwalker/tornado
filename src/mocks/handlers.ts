import { rest } from 'msw';

import type { Data, MessageData } from '../types/types';
import database from './database';

let pollingCount = 0;
let submitCount = 0;

export const handlers = [
  rest.get('/api/data', (req, res, ctx) =>
    res(
      ctx.status(200),
      ctx.delay(Math.random() * 1000),
      ctx.json<Data[]>(database.data.getAll()),
    ),
  ),
  rest.get('/api/error', (req, res, ctx) =>
    res(ctx.status(400), ctx.json<MessageData>(database.error.getAll()[0])),
  ),
  rest.get('/api/polling', (req, res, ctx) => {
    pollingCount = pollingCount + 1;
    if (pollingCount <= 2) {
      return res(
        ctx.status(200),
        ctx.delay(Math.random() * 5000),
        ctx.json<MessageData>(database.poll.getAll()[0]),
      );
    }

    return res(ctx.status(400), ctx.json({ message: 'Error!' }));
  }),
  rest.post('/api/name/', (req, res, ctx) => {
    submitCount = submitCount + 1;
    if (submitCount % 2 === 0) {
      return res(ctx.status(500), ctx.delay(Math.random() * 2000));
    }

    return res(ctx.status(201), ctx.delay(Math.random() * 2000));
  }),
];
