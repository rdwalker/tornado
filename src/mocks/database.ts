import { faker } from '@faker-js/faker';
import { factory, primaryKey } from '@mswjs/data';

const database = factory({
  data: {
    id: primaryKey(() => faker.datatype.uuid()),
    name: String,
  },
  error: {
    id: primaryKey(() => faker.datatype.uuid()),
    message: () => faker.lorem.sentence(),
  },
  poll: {
    id: primaryKey(() => faker.datatype.uuid()),
    message: () => 'Success!',
  },
});

for (let i = 0; i < 50; i += 1) {
  database.data.create({
    name: faker.hacker.phrase(),
  });
}

database.poll.create();
database.error.create();

export default database;
