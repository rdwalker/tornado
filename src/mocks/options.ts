import type { SharedOptions } from 'msw';

export const options: SharedOptions = {
  onUnhandledRequest: ({ method, url }) => {
    if (url.pathname.startsWith('/api')) {
      throw new Error(`Unhandled ${method} request to ${url.toString()}`);
    }
  },
};
