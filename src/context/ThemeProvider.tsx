import type { ReactElement, ReactNode } from 'react';
import { createContext, useContext, useMemo, useState } from 'react';

import type { PaletteMode } from '@mui/material';
import {
  createTheme,
  ThemeProvider as MUIThemeProvider,
  useMediaQuery,
} from '@mui/material';

interface ThemeContextValues {
  toggleColorMode: VoidFunction;
}

const ThemeContext = createContext<ThemeContextValues>({
  toggleColorMode: () => undefined,
});

function ThemeProvider({
  children,
  initialColorMode,
}: {
  children: ReactNode;
  initialColorMode?: PaletteMode;
}): ReactElement {
  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');
  const [mode, setMode] = useState<PaletteMode>(
    prefersDarkMode ||
      initialColorMode === 'dark' ||
      localStorage.getItem('theme') === 'dark'
      ? 'dark'
      : 'light',
  );

  if (initialColorMode !== undefined && initialColorMode !== mode) {
    setMode(initialColorMode);
  }

  const values: ThemeContextValues = useMemo(
    () => ({
      toggleColorMode: (): void => {
        setMode((prevMode) => {
          const newMode = prevMode === 'light' ? 'dark' : 'light';
          localStorage.setItem('theme', newMode);

          return newMode;
        });
      },
    }),
    [],
  );

  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode,
          primary: {
            main: '#3B9600',
          },
          secondary: {
            main: '#c7c7c7',
          },
        },
      }),
    [mode],
  );

  return (
    <ThemeContext.Provider value={values}>
      <MUIThemeProvider theme={theme}>{children}</MUIThemeProvider>
    </ThemeContext.Provider>
  );
}

export function useColorToggle(): ThemeContextValues {
  return useContext(ThemeContext);
}

export default ThemeProvider;
