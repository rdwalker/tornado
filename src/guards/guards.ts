function isResponse(res: unknown): res is Response {
  return res instanceof Response;
}

function isError(err: unknown): err is Error {
  return err instanceof Error;
}

function assertIsError(err: unknown): asserts err is Error {
  if (!(err instanceof Error)) {
    throw err;
  }
}

export { assertIsError, isError, isResponse };
