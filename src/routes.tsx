import { lazy, Suspense } from 'react';

import { createBrowserRouter } from 'react-router-dom';

import App from './App';
import ErrorElement from './components/ErrorElement/ErrorElement';
import LoadingElement from './components/LoadingElement/LoadingElement';
import type { Data, MessageData } from './types/types';
import buildLazyLoader from './utils/buildLazyLoader';

const HomePage = lazy(() => import('./components/Pages/HomePage/HomePage'));

const { LazyPage: LazyDataPage, lazyLoader: lazyDataLoader } = buildLazyLoader<
  Data[]
>({
  componentImportFn: () => import('./components/Pages/DataPage/DataPage'),
  loaderImportFn: () => import('./components/Pages/DataPage/dataPageLoader'),
});

const { LazyPage: LazyErrorPage, lazyLoader: lazyErrorLoader } =
  buildLazyLoader<MessageData>({
    componentImportFn: () => import('./components/Pages/ErrorPage/ErrorPage'),
    loaderImportFn: () =>
      import('./components/Pages/ErrorPage/errorPageLoader'),
  });

const { LazyPage: LazyPollingPage, lazyLoader: lazyPollingLoader } =
  buildLazyLoader<MessageData>({
    componentImportFn: () =>
      import('./components/Pages/PollingPage/PollingPage'),
    loaderImportFn: () =>
      import('./components/Pages/PollingPage/pollingPageLoader'),
  });

const router = createBrowserRouter(
  [
    {
      path: '/',
      element: <App />,
      errorElement: <ErrorElement />,
      children: [
        {
          errorElement: <ErrorElement />,
          children: [
            {
              index: true,
              element: (
                <Suspense fallback={<LoadingElement />}>
                  <HomePage />
                </Suspense>
              ),
            },
            {
              path: '/data',
              element: <LazyDataPage />,
              loader: lazyDataLoader,
            },
            {
              path: '/error',
              element: <LazyErrorPage />,
              loader: lazyErrorLoader,
            },
            {
              path: '/poll',
              element: <LazyPollingPage />,
              loader: lazyPollingLoader,
            },
            { path: '*', element: <HomePage /> },
          ],
        },
      ],
    },
  ],
  {
    basename: import.meta.env.BASE_URL,
  },
);

export { router };
