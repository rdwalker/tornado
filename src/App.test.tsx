import { render, screen } from '@testing-library/react';
import { RouterProvider } from 'react-router-dom';

import App from './App';
import { router } from './routes';

describe('App', () => {
  it('renders without error', () => {
    render(<App />, { wrapper: () => <RouterProvider router={router} /> });
    expect(screen.getByText('Tornado')).toBeVisible();
  });
});
