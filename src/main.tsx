import type { ReactElement } from 'react';
import { StrictMode } from 'react';

import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import { CssBaseline } from '@mui/material';
import { QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { extend } from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import utc from 'dayjs/plugin/utc';
import { createRoot } from 'react-dom/client';
import { Toaster } from 'react-hot-toast';
import { RouterProvider } from 'react-router-dom';

import './index.css';

import LoadingElement from './components/LoadingElement/LoadingElement';
import ToastBar from './components/Toasts/ToastBar';
import ThemeProvider from './context/ThemeProvider';
import { queryClient } from './queryClient';
import { router } from './routes';

extend(relativeTime);
extend(utc);

if (import.meta.env.DEV) {
  Promise.all([import('./mocks/browser'), await import('./mocks/options')])
    .then(([{ worker }, { options }]) => worker.start(options))
    .catch(() => {
      throw new Error('Failed to start MSW');
    });
}

const rootElement = document.getElementById('root');
if (!rootElement) {
  throw new Error('root element missing');
}

createRoot(rootElement).render(
  <StrictMode>
    <ThemeProvider>
      <CssBaseline />
      <QueryClientProvider client={queryClient}>
        <RouterProvider fallbackElement={<LoadingElement />} router={router} />
        <ReactQueryDevtools position="bottom-right" />
      </QueryClientProvider>
      <Toaster position="bottom-left" reverseOrder>
        {(t): ReactElement => <ToastBar toast={t} />}
      </Toaster>
    </ThemeProvider>
  </StrictMode>,
);
