import type { QueryKey, QueryOptions } from '@tanstack/react-query';
import type { LoaderFunctionArgs } from 'react-router-dom';
import { defer } from 'react-router-dom';

import { queryClient } from '../queryClient';

function createLoader<T>(
  rootQueryKey: QueryKey,
  shouldDefer = false,
  queryOptions?: Omit<QueryOptions<T>, 'queryKey'>,
): (
  loaderFunctionArgs: LoaderFunctionArgs,
) => Promise<ReturnType<typeof defer>> {
  return async ({ params, request }) => {
    const searchParams = new URL(request.url).searchParams;
    // Filter out client-side state variables
    // Only send server-relevant query parameters
    // This is done to avoid introducing client-side query parameters to the
    // React-Query queryKey so that the cache is accessed correctly if only
    // Client-side search params change.
    const queryKeyParams = [...searchParams.entries()].filter(([param]) =>
      ['page', 'pageSize', 'sortModel', 'filterModel'].includes(param),
    );

    const routeKey: QueryKey = [...rootQueryKey, ...Object.values(params)];
    const queryKey =
      queryKeyParams.length === 0 ? [routeKey] : [routeKey, queryKeyParams];
    const pollingDataPromise = queryClient.ensureQueryData<T>(
      queryKey,
      queryOptions,
    );

    return defer({
      data: shouldDefer ? pollingDataPromise : await pollingDataPromise,
    });
  };
}

export { createLoader };
