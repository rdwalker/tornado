// https://gist.github.com/brophdawg11/03a475e26922e09aa35ca8b5900a4fb4
import type { ReactElement } from 'react';
import { lazy, Suspense } from 'react';

import type { LoaderFunction } from 'react-router-dom';

import LoadingElement from '../components/LoadingElement/LoadingElement';

interface BuildLazyLoaderArgs {
  componentImportFn: () => Promise<{ readonly default: () => ReactElement }>;
  loaderImportFn: () => Promise<{ readonly default: LoaderFunction }>;
}
function buildLazyLoader<T>({
  componentImportFn,
  loaderImportFn,
}: BuildLazyLoaderArgs): {
  LazyPage: () => ReactElement;
  lazyLoader: LoaderFunction;
} {
  let Component: ReturnType<typeof lazy> | (() => ReactElement) =
    lazy(componentImportFn);

  function LazyPage(): ReactElement {
    return (
      <Suspense fallback={<LoadingElement />}>
        <Component />
      </Suspense>
    );
  }

  const lazyLoader: LoaderFunction = async (...args): Promise<T> => {
    const controller = new AbortController();

    void componentImportFn().then((componentModule) => {
      if (!controller.signal.aborted) {
        Component = componentModule.default;
      }
    });

    try {
      const { default: loader } = await loaderImportFn();

      return await loader(...args);
    } finally {
      controller.abort();
    }
  };

  return { LazyPage, lazyLoader };
}

export default buildLazyLoader;
