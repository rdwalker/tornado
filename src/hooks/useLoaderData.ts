import { useLoaderData as useOriginalLoaderData } from 'react-router-dom';

const useLoaderData = useOriginalLoaderData as <
  T,
  P extends boolean = false,
>() => P extends false
  ? Awaited<{ data: T }>
  : Awaited<{ data: T }> | undefined;

export default useLoaderData;
