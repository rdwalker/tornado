import { useMemo, useState } from 'react';

interface UseToggleReturn {
  value: boolean;
  setValueTrue: VoidFunction;
  setValueFalse: VoidFunction;
  toggleValue: VoidFunction;
}

function useToggle(initialValue = false): UseToggleReturn {
  const [value, setValue] = useState(initialValue);

  return useMemo(
    () => ({
      value,
      setValueTrue: (): void => {
        setValue(true);
      },
      setValueFalse: (): void => {
        setValue(false);
      },
      toggleValue: (): void => {
        setValue((prev) => !prev);
      },
    }),
    [value],
  );
}

export default useToggle;
