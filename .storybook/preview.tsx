import { JSXElementConstructor, ReactElement } from 'react';

import { DocsContainer } from '@storybook/addon-docs';
import { StoryFn } from '@storybook/react';
import { themes } from '@storybook/theming';
import { useDarkMode } from 'storybook-dark-mode';

import ThemeProvider from '../src/context/ThemeProvider';
import theme from './theme';

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  docs: {
    container: (props) => {
      const isDark = useDarkMode();

      return (
        <DocsContainer {...props} theme={isDark ? themes.dark : themes.light} />
      );
    },
  },
  darkMode: {
    dark: {
      ...themes.dark,
      brandTitle: theme.brandTitle,
      booleanBg: 'rgba(255, 255, 255, 0.15)',
      booleanSelectedBg: 'rgb(47,47,47)',
      buttonBg: 'rgb(68,68,68)',
      buttonBorder: 'rgba(255, 255, 255, 0.15)',
    },
    light: {
      ...themes.normal,
      brandTitle: theme.brandTitle,
      booleanBg: 'rgba(0, 0, 0, 0.15)',
      booleanSelectedBg: 'rgb(246, 249, 252)',
      buttonBg: 'rgb(250, 250, 250)',
      buttonBorder: 'rgba(0, 0, 0, 0.15)',
    },
    current: 'dark',
    stylePreview: true,
  },
};

function ThemeWrapper({ children }): ReactElement {
  const isDark = useDarkMode();

  return (
    <ThemeProvider initialColorMode={isDark ? 'dark' : 'light'}>
      {children}
    </ThemeProvider>
  );
}

export const decorators = [
  (Story: StoryFn<JSXElementConstructor<unknown>>) => {
    return (
      <ThemeWrapper>
        <Story />
      </ThemeWrapper>
    );
  },
];
