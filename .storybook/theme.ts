import { create } from '@storybook/theming';

export default create({
  base: 'light',
  brandTitle: 'Tornado',
  brandTarget: '_self',
});
