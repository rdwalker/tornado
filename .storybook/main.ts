import { StorybookConfig } from '@storybook/react-vite';

const base = process.env.BASE_URL ? `${process.env.BASE_URL}storybook/` : '';
const config: StorybookConfig = {
  staticDirs: ['../public'],
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: [
    '@storybook/addon-links',
    '@storybook/addon-essentials',
    '@storybook/addon-interactions',
    '@storybook/addon-a11y',
    'storybook-dark-mode',
  ],
  framework: {
    name: '@storybook/react-vite',
    options: {},
  },
  core: {
    disableTelemetry: true,
  },
  features: {
    storyStoreV7: true,
  },
  docs: {
    autodocs: true,
  },
  async viteFinal(config, { configType }) {
    if (configType === 'PRODUCTION') {
      return {
        ...config,
        base: base,
        build: {
          ...config.build,
          sourcemap: false,
        },
      };
    }
    return config;
  },
};
module.exports = config;
