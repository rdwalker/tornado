import react from '@vitejs/plugin-react';
import * as fs from 'fs';
import * as path from 'path';
import type { PluginOption } from 'vite';
import { configDefaults, defineConfig } from 'vitest/config';

function excludeMsw(): PluginOption {
  return {
    name: 'exclude-msw',
    resolveId(source: string): string | null {
      return source === 'virtual-module' ? source : null;
    },
    renderStart(outputOptions): void {
      const outDir = outputOptions.dir;
      if (typeof outDir !== 'string') {
        throw new Error('outDir is not a string');
      }
      const msWorker = path.resolve(outDir, 'mockServiceWorker.js');
      // eslint-disable-next-line no-console
      fs.rm(msWorker, () => console.log(`Deleted ${msWorker}`));
    },
  };
}

export default defineConfig({
  base: process.env.BASE_URL ?? '/',
  plugins: [react(), excludeMsw()],
  server: {
    port: 3000,
    strictPort: true,
  },
  resolve: {
    alias: {
      src: path.resolve(__dirname, 'src'),
    },
  },
  test: {
    globals: true,
    mockReset: true,
    clearMocks: true,
    environment: 'jsdom',
    setupFiles: './setupTests.ts',
    coverage: {
      ...configDefaults.coverage,
      provider: 'c8',
      include: ['src/**'],
      exclude: [
        ...(configDefaults.coverage.exclude ?? []),
        'src/main.tsx',
        'src/mocks/**',
        'src/utils/testUtils/**',
        'src/**/*.stories.tsx',
      ],
    },
  },
});
